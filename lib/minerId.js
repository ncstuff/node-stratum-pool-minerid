/* minerId
Attempts to proxy the GBT request in pools to append minerID details
*/
var bitcore = require('bitcore-lib-cash');
var merkleTree = require('./merkleTree.js');
var util = require('./util.js');
var bitcoinMessage = require('bitcore-message');

exports.getTransactionBuffers = function(txs){
    var txHashes = txs.map(function(tx){
        if (tx.txid !== undefined) {
            return util.uint256BufferFromHash(tx.txid);
        }
        return util.uint256BufferFromHash(tx.hash);
    });
    return [null].concat(txHashes);
}

exports.getMerkleHashes = function(steps){
    return steps.map(function(step){
        return step.toString('hex');
    });
}
exports.serializeHeader = function(rpcData, merkleRoot, nTime, nonce){

    var header =  new Buffer(80);
    var position = 0;
    header.write(nonce, position, 4, 'hex');
    header.write(rpcData.bits, position += 4, 4, 'hex');
    header.write(nTime, position += 4, 4, 'hex');
    header.write(merkleRoot, position += 4, 32, 'hex');
    header.write(rpcData.previousblockhash, position += 32, 32, 'hex');
    header.writeUInt32BE(rpcData.version, position + 32);
    var header = util.reverseBuffer(header);
    return header;
};

exports.signMessage = function(privKey, message){ 
    var message = new bitcoinMessage(message);
    return message.sign(privKey);
}
exports.modifyBlockHeader = function(rpcData, address, privKeyString ){
    //prepend addr hash
    var nonce = "0";
    var nTime = "0";
    
    var privateKey = new bitcore.PrivateKey(privKeyString);
    var pubKey = new bitcore.PublicKey( privateKey );
    var hash = util.sha256d(pubKey.toString())
   
    var txHashs = exports.getTransactionBuffers(rpcData.transactions);
    
    
    var _merkleTree = new merkleTree(txHashs);    
    var merkleRoot = util.reverseBuffer(_merkleTree.withFirst(hash)).toString('hex');
    //console.log(`MERKLE ROOT\n${merkleRoot}`);
    var headerBuffer = exports.serializeHeader(rpcData, merkleRoot, nTime, nonce);
    var moddedHeaderHash = util.sha256d(headerBuffer, parseInt(nTime, 16)).toString('hex');
    
    var signedHash = exports.signMessage( privateKey , moddedHeaderHash);
    return [signedHash,moddedHeaderHash];
    /*
    TO allow an RPC call here, requires Promisfying the entire stack but up to the pool init...
    So lets not do that..

    return new Promise((callback,reject)=>{
        daemon.cmd('signmessage',[pubKey, moddedHeaderHash], 
        function(result){
            if (result.error){
                console.log('signmessage call failed for daemon instance ' +
                    result.instance.index + ' with error ' + JSON.stringify(result.error));
                reject(result.error);
            } else {                
                callback([result.response, moddedHeaderHash]);
                //callback = function(){};
            }
        })
    })   
    */
}

exports.complieMinerId = function(rpcData,addr, privKey){
    const OP_PUSHDATA4 = 0x4e;
    let SCRIPT = null;
    
   // var pubkey = "mjfJL3XgzL6eWEgnmtQcfFTwWVfc4U66Y8";//exports.scriptToPubKey(addr);
    var signature = exports.modifyBlockHeader(rpcData, addr, privKey)

        var opReturn =  {
            publicKey : addr,
            headerHash : signature[1],
            headerSignature : signature[0],// signed header hash of the modded merkle tree
            ruleSets: {
                "segwit" : 0,
                "bigBlocks" : 1
            },
            signals : {
                "bacon" : 1
            },
            metaData : {
                comment: "I like BigBlocks",
                domain: "bitcoincash.org"
            }
        }
        
       // console.log(`OP RETURN \n`,opReturn);
       // console.log(`OP HEX\n`,new Buffer(JSON.stringify(opReturn)).toString('hex') );
        //console.log(`OP serialise\n`,util.serializeString(JSON.stringify(opReturn)));
        let jsonString = JSON.stringify(opReturn);
        let jsonBuff = new Buffer(jsonString);
        let jsonBuffLength = jsonBuff.byteLength;
        
        if(jsonBuffLength > 520){
            //Split into chunks
            let chunkCount = Math.round(jsonBuffLength / 520);
            let chunks = [];
            let startSlice = 0;
            const MAX_BYTES = 520;
            
            for(let i = 0; i < chunkCount; i++){
                let cap = startSlice+MAX_BYTES
                if( cap > jsonBuffLength ){
                    cap = jsonBuffLength;    // Not inclusive
                }
                
                let _thisChunk = jsonBuff.slice(startSlice, cap)
                
                //console.log("This chunk length:",_thisChunk.byteLength);
                let thisChunkBytes = Buffer.allocUnsafe(4);
                thisChunkBytes.writeInt32LE(_thisChunk.byteLength);
                chunks.push(
                    Buffer.concat([
                        Buffer.from([OP_PUSHDATA4]),
                        thisChunkBytes,
                        _thisChunk
                    ])
                )
                startSlice = cap;                
            }
            SCRIPT = Buffer.concat(chunks);
        }else{
            let bytePush = Buffer.allocUnsafe(4);
            bytePush.writeInt32LE(jsonBuffLength);
            console.log(bytePush.byteLength);
            console.log(bytePush)
        
        SCRIPT = Buffer.concat([
            Buffer.from([OP_PUSHDATA4]),
            bytePush,
            jsonBuff
        ]);
        }
       
        return SCRIPT; 
        
    
}
exports.minerIdToScript = function(rpcData,addr, privKey){
   
    
   
    var minerId = exports.complieMinerId(rpcData,addr, privKey);
            const OP_RETURN = 0x6a;
            const OP_PUSH4BYTES = 0x04;
            const OP_PUSH1BYTES = 0x01;
            const LOKADID = Buffer.allocUnsafe(4);
            LOKADID.writeUInt32LE(0x0000002a,0);
            const OP_PUSHDATA1 = 0x4c;
            const JSON_ENCODED_TYPE = 0x00;
           
            const SCRIPT_CONST = Buffer.concat([
                Buffer.from([OP_RETURN, OP_PUSH4BYTES]),
                LOKADID,
                Buffer.from([OP_PUSH1BYTES, JSON_ENCODED_TYPE, OP_PUSH1BYTES])
            ])
            let SCRIPT = null;
           
           // console.log(`BUFFER \n`,Buffer.concat([new Buffer([0x6a, 0x4c, 0x2A, 0x4c, 0x31, 0x4e, 0x31])] , minerId));
           let chunks = Buffer.from([0x01]);
           
           
            SCRIPT = Buffer.concat([SCRIPT_CONST,chunks,minerId]);
            
            return SCRIPT
            /*
            0 field is OP_RETURN 0x6a
            first field is a protocol identifier (as per joannes' spec). 0x4e 0x2A (0x4e PUSH_DATA(1) 0x2A [42 - the answer to everything] )
            second field is the number of data chunks (push data can only 520 bytes so if it's larger we have to split it) 0x4c 1? PUSH_DATA(1) 1 chunk 
            3rd field is an integer denoting encoding e.g. json=0 protos=1, custom=2 0x4c 1 (Push data -> JSON encoding)
            4th and subsequent fields are the actual data (in this case utf8 encoded JSON.  
                The first field of the encoded data should always be a version number (just an integer) so we can handle upgrades to the data format. 
            */
};
exports.scriptToPubKey = function(payScript){
    return payScript.slice(3,-2);    
}

exports.byteLength = function (str) {
    // returns the byte length of an utf8 string
    var str = str.toString();
    var s = str.length;
    for (var i=str.length-1; i>=0; i--) {
      var code = str.charCodeAt(i);
      if (code > 0x7f && code <= 0x7ff) s++;
      else if (code > 0x7ff && code <= 0xffff) s+=2;
      if (code >= 0xDC00 && code <= 0xDFFF) i--; //trail surrogate
    }
    return s;
  }