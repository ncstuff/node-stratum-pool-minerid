## MinerID PoC node-stratum-pool

Forked from https://github.com/zone117x/node-stratum-pool 

This is a Proof Of Concept and should probably not be used in production.

In order to run this successfully you will also need a new field in your pool conf.

```
{
coin : "coin conf obj",
address :  "", 
    //Address to where block rewards are given   
privKey : "" 
    // NEW FIELD: Private Key of the address above. This is required to sign the blockhash that MinerID creates.

 ....
}
```


To run an exmaple pool:
1. clone `https://bitbucket.org/ncstuff/minerid-testnet-pool.git`
2. run npm install (It may ask you for a user / pass, if it does you can probably skip step3)
3. clone this project into node_modules of `minerid-testnet-pool`
4. Setup your testnet node as per `start.js` config